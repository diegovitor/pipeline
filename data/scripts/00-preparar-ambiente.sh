#!/usr/bin/env bash


# Instalação de pacotes básicos
sudo apt-get update -y
sudo apt-get install unzip vim jq apt-transport-https lsb-release ca-certificates curl gnupg -y

# Instalação do Docker Community Edition
echo "Docker Install Beginning..."
echo "Uninstall old versions"
sudo apt-get remove docker docker.io containerd runc
sudo apt-get update -y

#Adicione a chave GPG oficial do Docker:
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg

# Use o seguinte comando para configurar o repositório:
echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update -y

#Instale o Docker Engine, o containerd e o Docker Compose.
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin -y
sudo docker --version
# sudo systemctl enable docker
# sudo systemctl status docker


# Configura o Docker para rodar com o usuário vagrant
sudo usermod -aG docker vagrant
sudo echo "vagrant   ALL=(ALL:ALL) NOPASSWD:ALL" >> /etc/sudoers

------------------------------------------  GITLAB-RUNNER  ----------------------------------------------------------------

# Instalar o Gitlab-Runner
sudo curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt-get install gitlab-runner -y

# Inserir usuário gitlab-runner ao grupo docker
sudo usermod -aG docker gitlab-runner

# Adicionar o gitlab-runner ao sudo sem pedir senha
sudo echo "gitlab-runner    ALL=(ALL:ALL) NOPASSWD:ALL" >> /etc/sudoers

# Executa o comando docker compose para baixar e executar o docker do gitlab


# Mudar executar o docker compose para instalar o container do gitlab
cd /mnt/vagrant/scripts/
docker compose -f docker-compose-gitlab.yml -p gitlab_project up -d