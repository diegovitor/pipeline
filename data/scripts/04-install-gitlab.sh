#!/bin/bash

sudo mkdir -p /storage/docker-homol/deploy/gitlab/{data,logs,config}

GITLAB_HOME=$HOME/data 

docker run -dit \
  --publish 443:443 --publish 80:80 --publish 2222:22 \
  --name gitlab \
  --hostname gitlab.local \
  --add-host=host.docker.internal:host-gateway \
  --restart always \
  --volume $GITLAB_HOME/data:/var/opt/gitlab \
  --volume $GITLAB_HOME/logs:/var/log/gitlab \
  --volume $GITLAB_HOME/config:/etc/gitlab \
  --shm-size 512m \
  gitlab/gitlab-ce:latest
  # gitlab/gitlab-ce:14.7.6-ce.0

# REFERENCIA: https://docs.gitlab.com/ee/install/docker.html


