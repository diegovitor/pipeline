#!/bin/bash

# Ingressar o Runner em modo shell no GitLab
# Se estiver utilizando gitlab.com ou certificado, altere para HTTPS://
sudo gitlab-runner register -n \
  --url https://gitlab.com/ \
  --registration-token $ENV_RUNNER_TOKEN \
  --executor shell \
  --description "Runner Shell"

# caso o runner não atualize no gitlab, reiniciar o serviço
# sudo systemctl restart gitlab-runner
# sudo systemctl enable gitlab-runner

# export ENV_RUNNER_TOKEN=<token runner do meu gitlab>